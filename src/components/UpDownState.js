import { Component } from "react";

class UpdownState extends Component{
    constructor(props){
        super(props);
        this.state = {
            count: 0
        }
    }
    onBtnUpClick =() =>{
        this.setState({
            count: this.state.count + 1
        })
    }

    onBtnDownClick =() =>{
        this.setState({
            count: this.state.count - 1
        })
    }
    render(){
        return(
            <div className="container mt-5 text-center">
                <div>
                    <div>
                        <h3>Count Number: {this.state.count}</h3>
                    </div>
                    <div>
                        <button  onClick={this.onBtnUpClick}><i className="fas fa-arrow-up fa-3x"></i></button>
                        <button  onClick={this.onBtnDownClick}><i className="fas fa-arrow-down fa-3x"></i></button>
                    </div>
                </div>
            </div>
        )
    }
}
export default UpdownState;
