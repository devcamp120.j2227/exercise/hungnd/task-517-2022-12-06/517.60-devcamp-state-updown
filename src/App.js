import '@fortawesome/fontawesome-free/css/all.min.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import UpdownState from './components/UpDownState';

function App() {
  return (
    <div>
      <UpdownState/>
    </div>
  );
}

export default App;
